﻿using NUnit.Framework;

namespace CowTools.Extensions.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class StringExtensionsTests
    {
        [Test]
        public void NullToEmpty()
        {
            string nullStr = null;
            Assert.AreEqual(string.Empty, nullStr.NullToEmpty());
            Assert.AreEqual(string.Empty, string.Empty.NullToEmpty());
            Assert.AreEqual(" ", " ".NullToEmpty());
            Assert.AreEqual("\n", "\n".NullToEmpty());
            Assert.AreEqual("Foo", "Foo".NullToEmpty());
        }


        [Test]
        public void EmptyToNull()
        {
            string nullStr = null;
            Assert.IsNull(nullStr.EmptyToNull());
            Assert.IsNull(string.Empty.EmptyToNull());
            Assert.AreEqual(" ", " ".EmptyToNull());
            Assert.AreEqual("\n", "\n".EmptyToNull());
            Assert.AreEqual("Foo", "Foo".EmptyToNull());
        }
    }
}
