﻿namespace CowTools.Extensions
{
    /// <summary>
    /// Extension methods for strings.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Convert empty strings to null.
        /// </summary>
        /// <param name="aString">String to sanitize.</param>
        /// <returns>Input string if it is not null or empty; null if it is.</returns>
        public static string EmptyToNull(this string aString)
            => string.IsNullOrEmpty(aString) ? null : aString;


        /// <summary>
        /// Convert null strings to empty.
        /// </summary>
        /// <param name="aString">String to sanitize.</param>
        /// <returns>Input string if it is not null; empty string if it is.</returns>
        public static string NullToEmpty(this string aString)
            => aString ?? string.Empty;
    }
}
