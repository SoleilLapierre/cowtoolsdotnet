﻿using CowTools.Expressions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CowTools.Test.Expressions
{
    [TestFixture]
    [SetCulture("en-US")]
    public class ArithmeticTreeTests
    {
        [Test]
        public void Empty()
        {
            var root = new ArithmeticTree();
            Assert.Throws<InvalidOperationException>(() => root.Evaluate());
        }


        [Test]
        public void Constant()
        {
            var root = new ArithmeticTree() { Value = 1 };
            Assert.AreEqual(1, root.Evaluate());
        }


        [Test]
        public void Variable()
        {
            var root = new ArithmeticTree() { Variable = "y" };
            Assert.AreEqual(2, root.Evaluate(_variables));
        }


        [Test]
        public void VariableNoTable()
        {
            var root = new ArithmeticTree() { Variable = "y" };
            Assert.Throws<InvalidOperationException>(() => root.Evaluate());
        }


        [Test]
        public void Expression()
        {
            var left21 = new ArithmeticTree() { Value = 2.0 };
            var left22 = new ArithmeticTree() { Variable = "z" };
            var left = new ArithmeticTree() { Operator = "^", Left = left21, Right = left22 };
            var right21 = new ArithmeticTree() { Variable = "x" };
            var right22 = new ArithmeticTree() { Variable = "y" };
            var right = new ArithmeticTree() { Operator = "-", Left = right21, Right = right22 };
            var root = new ArithmeticTree() { Operator = "*", Left = left, Right = right };
            Assert.AreEqual(-8, root.Evaluate(_variables));
        }


        [Test]
        public void ParsePostfix()
        {
            var expr = ArithmeticTree.ParsePostfix("2 4 ^ 7 - z /");
            Assert.AreEqual(3, expr.Evaluate(_variables));
        }



        [Test]
        public void ParsePostfixAllOperatorsAtEnd()
        {
            var expr = ArithmeticTree.ParsePostfix("25 2 4 ^ -");
            Assert.AreEqual(9, expr.Evaluate(_variables));
        }


        [Test]
        public void ParseInfixSimple()
        {
            var expr = ArithmeticTree.Parse("y^4 - 7");
            Assert.AreEqual(9, expr.Evaluate(_variables));
        }


        [Test]
        public void ParseInfix()
        {
            var expr = ArithmeticTree.Parse("(2^4 - 7) / (6 / y)");
            Assert.AreEqual(3, expr.Evaluate(_variables));
        }


        [Test]
        public void LogicExpressions()
        {
            var expr = ArithmeticTree.Parse("x | false");
            Assert.AreEqual(1, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("x & false");
            Assert.AreEqual(0, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("x & true");
            Assert.AreEqual(1, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("x !| false");
            Assert.AreEqual(0, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("x = 1");
            Assert.AreEqual(1, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("z > 2.5");
            Assert.AreEqual(1, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("(z > 2.5) & (x | 0 | 1)");
            Assert.AreEqual(1, expr.Evaluate(_variables));

            expr = ArithmeticTree.Parse("(z <= 2.5) & (x | 0 | 1)");
            Assert.AreEqual(0, expr.Evaluate(_variables));
        }


        private static IReadOnlyDictionary<string, double> _variables = new Dictionary<string, double> {
            { "x", 1.0 },
            { "y", 2.0 },
            { "z", 3.0 },
        };
    }
}
