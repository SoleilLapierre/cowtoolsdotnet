﻿using CowTools.Collections;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace CowTools.Test.Collections
{
    [TestFixture]
    [SetCulture("en-US")]
    public class LayeredDictionaryTests
    {
        [Test]
        public void Shadowing()
        {
            var dict = new LayeredDictionary<string, int>();
            dict["x"] = 1;
            Assert.AreEqual(2, dict.AddLayer());
            dict["x"] = 2;
            Assert.AreEqual(2, dict["x"]);
            dict.SetAtLayer(0, "x", 3);
            Assert.AreEqual(2, dict["x"]);
            Assert.AreEqual(1, dict.RemoveLayer());
            Assert.AreEqual(3, dict["x"]);
        }


        [Test]
        public void Keys()
        {
            var dict = new LayeredDictionary<string, int>
            {
                ["x"] = 1,
                ["y"] = 2
            };

            Assert.AreEqual(2, dict.AddLayer());
            dict["x"] = 3;
            dict["z"] = 4;

            var sorted = dict.Keys.ToList();
            sorted.Sort();
            Assert.IsTrue(sorted.SequenceEqual(new List<string>() { "x", "y", "z" }));
        }
    }
}
