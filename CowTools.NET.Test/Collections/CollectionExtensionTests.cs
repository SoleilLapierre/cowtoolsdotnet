﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CowTools.Collections.Tests
{
    [TestFixture]
    [SetCulture("en-US")]
    public class CollectionExtensionTests
    {
        [Test]
        public void AddRange_Numbers()
        {
            TestAddRange(_testNums1, _testNums2);
        }


        [Test]
        public void AddRange_Strings()
        {
            TestAddRange(_testStrings1, _testStrings2);
        }


        [Test]
        public void AddRange_Objects()
        {
            TestAddRange(_testObjs1, _testObjs2);
        }


        private void TestAddRange<T>(IEnumerable<T> aInitialItems, IEnumerable<T> aNewItems)
        {
            ICollection<T> collection = aInitialItems.ToList();
            collection.AddRange(aNewItems);

            Assert.AreEqual(aInitialItems.Count() + aNewItems.Count(), collection.Count);
            foreach (var item in aInitialItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }
        }


        [Test]
        public void AddIfUnique_Numbers()
        {
            TestAddIfUnique(_testNums1, _testNums2);
        }


        [Test]
        public void AddIfUnique_Strings()
        {
            TestAddIfUnique(_testStrings1, _testStrings2);
        }


        [Test]
        public void AddIfUnique_Objects()
        {
            TestAddIfUnique(_testObjs1, _testObjs2);
        }


        private void TestAddIfUnique<T>(IEnumerable<T> aInitialItems, IEnumerable<T> aNewItems)
        {
            ICollection<T> collection = aInitialItems.ToList();
            
            foreach (var item in aInitialItems)
            {
                Assert.IsFalse(collection.AddIfUnique(item));
            }

            Assert.AreEqual(aInitialItems.Count(), collection.Count);
            foreach (var item in aInitialItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsFalse(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsTrue(collection.AddIfUnique(item));
            }

            Assert.AreEqual(aInitialItems.Count() + aNewItems.Count(), collection.Count);
            foreach (var item in aInitialItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }
        }


        [Test]
        public void AddUniqueFromRange_Numbers()
        {
            TestAddUniqueFromRange(_testNums1, _testNums2);
        }


        [Test]
        public void AddUniqueFromRange_Strings()
        {
            TestAddUniqueFromRange(_testStrings1, _testStrings2);
        }


        [Test]
        public void AddUniqueFromRange_Objects()
        {
            TestAddUniqueFromRange(_testObjs1, _testObjs2);
        }


        private void TestAddUniqueFromRange<T>(IEnumerable<T> aInitialItems, IEnumerable<T> aNewItems)
        {
            ICollection<T> collection = aInitialItems.ToList();

            Assert.IsTrue(aInitialItems.SequenceEqual(collection.AddUniqueFromRange(aInitialItems)));

            Assert.AreEqual(aInitialItems.Count(), collection.Count);
            foreach (var item in aInitialItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsFalse(collection.Contains(item));
            }

            Assert.AreEqual(0, collection.AddUniqueFromRange(aNewItems).Count);

            Assert.AreEqual(aInitialItems.Count() + aNewItems.Count(), collection.Count);
            foreach (var item in aInitialItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }

            foreach (var item in aNewItems)
            {
                Assert.IsTrue(collection.Contains(item));
            }
        }


        private class TestObject
        {
            public string Content { get; set; }

            public override int GetHashCode()
            {
                return (Content ?? "").GetHashCode();
            }

            public override bool Equals(object aOther)
            {
                if (!(aOther is TestObject obj))
                {
                    return false;
                }

                if (Content is null)
                {
                    return obj.Content is null;
                }

                return Content == obj.Content;
            }
        }


        private int[] _testNums1 = { 1, 2, 3 };
        private int[] _testNums2 = { 4, 5 };

        private string[] _testStrings1 = { "Foo", "Bar" };
        private string[] _testStrings2 = { "Baz", "Quux" };

        private TestObject[] _testObjs1 = { new TestObject { Content = "Foo" },
                                            new TestObject { Content = "Bar" }};

        private TestObject[] _testObjs2 = { new TestObject { Content = "Baz" },
                                            new TestObject { Content = "Quux" }};
    }
}
