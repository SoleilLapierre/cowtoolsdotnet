﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CowTools.Collections
{
    /// <summary>
    /// Multi-level dictionary with lookup fallback.
    /// Starts with one layer (the base or global layer). Added layers grow the stack "above" previous layers,
    /// and lookups start with the top layer and work downward until a match is found. 
    /// Inserting adds to the top layer by default, but there is a method to add to a specific layer.
    /// </summary>
    /// <typeparam name="TKey">Type of the lookup keys.</typeparam>
    /// <typeparam name="TValue">Type of the stored values.</typeparam>
    public class LayeredDictionary<TKey, TValue>: IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
    {
        /// <summary>
        /// Add a new layer at the top of the stack.
        /// </summary>
        /// <returns>The number of layers after the addition.</returns>
        public int AddLayer()
        {
            _layers.Insert(0, new Dictionary<TKey, TValue>());
            return _layers.Count;
        }


        /// <summary>
        /// Remove the layer at the top of the stack.
        /// </summary>
        /// <returns>The number of layers after the removal.</returns>
        /// <exception cref="InvalidOperationException">The last remaining layer cannot be removed.</exception>
        public int RemoveLayer()
        {
            return RemoveLayer(0);
        }


        /// <summary>
        /// Remove a specified layer. Removing the root layer will cause the next layer above it to become the new root.
        /// </summary>
        /// <param name="layer">Index of the layer to remove. Zero means the top layer; positive integers work down the stack towards the root layer.</param>
        /// <returns>The number of layers after the removal.</returns>
        /// <exception cref="InvalidOperationException">The last remaining layer cannot be removed.</exception>
        public int RemoveLayer(int layer)
        {
            if (_layers.Count == 1)
            {
                throw new InvalidOperationException("Cannot remove last layer. Use Clear() to empty the dictionary instead.");
            }

            _layers.RemoveAt(layer);
            return _layers.Count;
        }


        /// <summary>
        /// Get the current number of layers. Will always be at least 1.
        /// </summary>
        public int LayerCount => _layers.Count;


        /// <summary>
        /// Add a new value to a specific layer. The key must not already exist in the specified layer.
        /// </summary>
        /// <param name="layer">Index of the layer to add to. Zero is the top layer, and positive numbers move down toward the root.</param>
        /// <param name="key">Key to add.</param>
        /// <param name="value">Value to add.</param>
        public void AddToLayer(int layer, TKey key, TValue value)
        {
            _layers[_layers.Count - 1 - layer].Add(key, value);
        }


        /// <summary>
        /// Add or replace a value in a specified layer.
        /// </summary>
        /// <param name="layer">Index of the layer to change. Zero is the top layer, and positive numbers move down toward the root.</param>
        /// <param name="key">Key to add or replace.</param>
        /// <param name="value">New value for the key.</param>
        public void SetAtLayer(int layer, TKey key, TValue value)
        {
            _layers[_layers.Count - 1 - layer][key] = value;
        }


        /// <summary>
        /// Remove a key and value from a specified layer, if present.
        /// </summary>
        /// <param name="layer">Index of the layer to modify. Zero is the top layer, and positive numbers move down toward the root.</param>
        /// <param name="key">Key to remove.</param>
        /// <returns>True if the key was found and removed.</returns>
        public bool RemoveFromLayer(int layer, TKey key)
        {
            return _layers[_layers.Count - 1 - layer].Remove(key);
        }


        /// <summary>
        /// Return the contents as a normal dictionary. Each key is only present once, with the value from the highest layer with that key.
        /// </summary>
        /// <returns>The contents as a flat dictionary, taking shadowing into account.</returns>
        public Dictionary<TKey, TValue> Flatten()
        {
            var result = new Dictionary<TKey, TValue>();
            foreach (var layer in Enumerable.Reverse(_layers))
            {
                foreach (var item in layer)
                {
                    result.Add(item.Key, item.Value);
                }
            }

            return result;
        }


        #region -- IDictionary implementation --

        /// <summary>
        /// Get a value from the topmost layer that has the key, or set the value of that key at the top layer.
        /// </summary>
        /// <param name="key">The key to get or set.</param>
        /// <returns>(get) The value of the key at the topmost layer that has it. (set) Nothing; stores the key at the top layer.</returns>
        /// <exception cref="KeyNotFoundException"></exception>
        public TValue this[TKey key]
        {
            get
            {
                if (TryGetValue(key, out var value))
                {
                    return value;
                }

                throw new KeyNotFoundException($"The key '{key}' was not found in any layer of the dictionary.");
            }

            set
            {
                _layers[0][key] = value;
            }
        }


        /// <summary>
        /// All the keys present in the data structure.
        /// Keys that exist in multiple layers are returned only once.
        /// Ordering is not guaranteed or consistent; sorting the result is recommended.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                var keys = new HashSet<TKey>();
                foreach (var layer in _layers)
                {
                    keys.AddRange(layer.Keys);
                }

                return keys;
            }
        }


        /// <summary>
        /// All values present in the data structure.
        /// Where a key exists in multiple layers, only the value from the topmost layer is included.
        /// Ordering is not guaranteed or consistent.
        /// </summary>
        public ICollection<TValue> Values => Flatten().Values;


        /// <summary>
        /// The number of currently visible items in the data structure. This may be less than
        /// the total number of items, because shadowed keys are subtracted from the total.
        /// </summary>
        public int Count => Flatten().Count;


        /// <summary>
        /// This is not a read-only data structure.
        /// </summary>
        public bool IsReadOnly => false;


        /// <summary>
        /// Add a new item to the topmost layer.
        /// </summary>
        /// <param name="key">Key to add.</param>
        /// <param name="value">Value to associate with the key.</param>
        public void Add(TKey key, TValue value) => _layers[0].Add(key, value);


        /// <summary>
        /// Add a new item to the topmost layer.
        /// </summary>
        /// <param name="item">Key/Value pair to add.</param>
        public void Add(KeyValuePair<TKey, TValue> item) => this[item.Key] = item.Value;


        /// <summary>
        /// Empty the collection. This also eliminates all but one layer.
        /// </summary>
        public void Clear()
        {
            _layers.Clear();
            _layers.Add(new Dictionary<TKey, TValue>());
        }


        /// <summary>
        /// Determine if the collection contains a given ite.
        /// </summary>
        /// <param name="item">Item to check for.</param>
        /// <returns>True if any layer contains the key and has an equal value for that key.</returns>
        /// <remarks>A true result doesn't mean you will be able to look up the key and get the same value, because it might be shadowed.</remarks>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            if (TryGetValue(item.Key, out var value))
            {
                return item.Value.Equals(value);
            }

            return false;
        }


        /// <summary>
        /// Check if a given key exists in the collection.
        /// </summary>
        /// <param name="key">Key to test for.</param>
        /// <returns>True if the key is present in the collection at any layer.</returns>
        public bool ContainsKey(TKey key) => Keys.Contains(key);


        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="array">Array to copy contents to.</param>
        /// <param name="arrayIndex">Index to store the first element at.</param>
        /// <exception cref="System.NotImplementedException">Always throws.</exception>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => throw new System.NotImplementedException();


        /// <summary>
        /// Enumerate the contents of this container. Does not return shadowed values.
        /// </summary>
        /// <returns>An iterator for use in a foreach loop.</returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => GetEnumeratorImpl();


        /// <summary>
        /// Remove a given key from the collection. This removes it from all layers that have it.
        /// </summary>
        /// <param name="key">Key to remove.</param>
        /// <returns>True if the key was found and removed from any layer.</returns>
        public bool Remove(TKey key)
        {
            var result = false;
            foreach (var layer in _layers)
            {
                if (layer.ContainsKey(key))
                {
                    layer.Remove(key);
                    result = true;
                }
            }

            return result;
        }


        /// <summary>
        /// Remove a given item from the collection. This removes it from all layers that have both the same key and the same value.
        /// </summary>
        /// <param name="item">Item to remove.</param>
        /// <returns>True if the item was found and removed from any layer.</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            var result = false;
            foreach (var layer in _layers)
            {
                if (layer.Contains(item))
                {
                    layer.Remove(item.Key);
                    result = true;
                }
            }

            return result;
        }


        /// <summary>
        /// Attempt to find a key and get the associated value if found.
        /// </summary>
        /// <param name="key">Key to search for.</param>
        /// <param name="value">Return storage for the value if found. Will be defaulted otherwise.</param>
        /// <returns>True if the value was found on any layer.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            foreach (var layer in _layers)
            {
                if (layer.TryGetValue(key, out value))
                {
                    return true;
                }    
            }

            value = default;
            return false;
        }


        /// <summary>
        /// Enumerate the contents of this container. Does not return shadowed values.
        /// </summary>
        /// <returns>An iterator for use in a foreach loop.</returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumeratorImpl();

        #endregion

        #region -- IReadOnlyDictionary implementation --

        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Values;

        #endregion

        private IEnumerator<KeyValuePair<TKey, TValue>> GetEnumeratorImpl()
        {
            var seen = new HashSet<TKey>();
            foreach (var layer in _layers)
            {
                foreach (var pair in layer)
                {
                    if (!seen.Contains(pair.Key))
                    {
                        seen.Add(pair.Key);
                        yield return pair;
                    }
                }
            }
        }


        private readonly List<Dictionary<TKey, TValue>> _layers = new List<Dictionary<TKey, TValue>> { new Dictionary<TKey, TValue>() };
    }
}
