﻿using System;
using System.Collections.Generic;

namespace CowTools.Reflection
{
    /// <summary>
    /// Helper class for reflecting attributes.
    /// </summary>
    public static class AttributeFinder
    {
        /// <summary>
        /// Find all class-level attributes of a given type on a class type.
        /// </summary>
        /// <typeparam name="T">The type of attribute to find.</typeparam>
        /// <param name="instance">An instance of a class to search for class attributes on.</param>
        /// <param name="inherit">Whether to also search base types.</param>
        /// <returns>Zero or more matching attributes.</returns>
        public static IEnumerable<T> FindClassAttributes<T>(object instance, bool inherit = true) where T: Attribute
            => FindClassAttributes<T>(instance.GetType(), inherit);


        /// <summary>
        /// Find all class-level attributes of a given type on a class type.
        /// </summary>
        /// <typeparam name="T">The type of attribute to find.</typeparam>
        /// <param name="aClassType">The class type to search for attributes.</param>
        /// <param name="inherit">Whether to also search base types.</param>
        /// <returns>Zero or more matching attributes.</returns>
        public static IEnumerable<T> FindClassAttributes<T>(Type aClassType, bool inherit = true) where T: Attribute
        {
            foreach (var attr in aClassType.GetCustomAttributes(typeof(T), inherit))
            {
                if (typeof(T).IsAssignableFrom(attr.GetType()))
                {
                    yield return attr as T;
                }
            }
        }
    }
}
