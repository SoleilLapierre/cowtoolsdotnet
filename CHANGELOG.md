# Version history

## v0.0.9, 2024-07-01

- Backed off framework version to NetStandard 2.0 to maintain .NET Framework compatibility for now.

## v0.0.8, 2023-09-17

- Added >= and <= operators to ArithmeticTree.

## v0.0.7, 2023-09-14

- Added boolean operators to ArithmeticTree.

## v0.0.6, 2023-09-13

- LayeredDictionary now implemented IReadOnlyDictionary.

## v0.0.5, 2023-09-12

- Added LayeredDictionary class.

## v0.0.4, 2023-09-07

- Updated target framework to .NET Standard 2.1.
- Added an arithmetic expression parser and evaluator.

## v0.0.3, 2020-11-16

- Added some extension methods for strings and XmlNodes.

## v0.0.2, 2020-04-03

- Added some extension methods for generic collections.

## v0.0.1, 2020-03-24

- First published version.
- Reflection helpers for finding types, attributes and implementations.
