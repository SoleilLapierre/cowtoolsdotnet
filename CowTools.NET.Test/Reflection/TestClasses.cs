﻿namespace CowTools.Reflection.Tests
{
    [Test1("Foo")]
    public class TestClass1
    {
    }

    [Test2("Bar")]
    public class TestClass2
    {
    }

    [Test1("Foo2")]
    [Test2("Bar2")]
    [Test3("Bar3", "Baz")]
    public class TestClass3
    {
    }

    [Test1("Foo3")]
    [Test2("Bar4")]
    public class TestClass4 : TestClass3
    {
    }


    public interface I1 { }

    public interface I2 : I1 { }

    public interface I3 { }

    public abstract class I1Abstract : I1 { }

    public class I1Impl : I1Abstract { }

    public class I2Impl : I2 { }

    public class I3Impl : I1, I3 { }

}
