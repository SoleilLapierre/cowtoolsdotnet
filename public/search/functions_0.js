var searchData=
[
  ['add_0',['add',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#a8de96fcffb298c5e216fedc22026a8ba',1,'CowTools.Collections.LayeredDictionary.Add(TKey key, TValue value)'],['../classCowTools_1_1Collections_1_1LayeredDictionary.html#aaafdef587bce462ea70c8394a7eebc2d',1,'CowTools.Collections.LayeredDictionary.Add(KeyValuePair&lt; TKey, TValue &gt; item)']]],
  ['addifunique_3c_20t_20_3e_1',['AddIfUnique&lt; T &gt;',['../classCowTools_1_1Collections_1_1CollectionExtensions.html#a1c79678add9dabd4c49870c11f408643',1,'CowTools::Collections::CollectionExtensions']]],
  ['addlayer_2',['AddLayer',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#a6d713d2c455720360c4c3fbf0c95dc09',1,'CowTools::Collections::LayeredDictionary']]],
  ['addrange_3c_20t_20_3e_3',['AddRange&lt; T &gt;',['../classCowTools_1_1Collections_1_1CollectionExtensions.html#a6eb4a04e228ae6ab3f464c417c6c6ac3',1,'CowTools::Collections::CollectionExtensions']]],
  ['addtolayer_4',['AddToLayer',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#adc508a8337c8bb74f81ce2b58b344bdc',1,'CowTools::Collections::LayeredDictionary']]],
  ['adduniquefromrange_3c_20t_20_3e_5',['AddUniqueFromRange&lt; T &gt;',['../classCowTools_1_1Collections_1_1CollectionExtensions.html#a1cac72106008726c1d966ce4d060f76a',1,'CowTools::Collections::CollectionExtensions']]]
];
