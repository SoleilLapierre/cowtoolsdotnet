﻿using System.Collections.Generic;

namespace CowTools.Collections
{
    /// <summary>
    /// Some useful extension methods for collections.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Add many items to a collection.
        /// </summary>
        /// <typeparam name="T">Type of item stored in the collection.</typeparam>
        /// <param name="aCollection">The existing collection to add items to.</param>
        /// <param name="aNewItems">The collection of new items to add.</param>
        public static void AddRange<T>(this ICollection<T> aCollection, IEnumerable<T> aNewItems)
        {
            foreach (var item in aNewItems)
            {
                aCollection.Add(item);
            }
        }


        /// <summary>
        /// Add a new item to a collection if it is not already present, using the default
        /// identity rules of the collection and type.
        /// </summary>
        /// <typeparam name="T">Type contained in the collection.</typeparam>
        /// <param name="aCollection">Existing collection to add to.</param>
        /// <param name="aNewItem">New item to add to the collection.</param>
        /// <returns>True if the item was added to the collection; false if it was
        /// already present.</returns>
        public static bool AddIfUnique<T>(this ICollection<T> aCollection, T aNewItem)
        {
            if (!aCollection.Contains(aNewItem))
            {
                aCollection.Add(aNewItem);
                return true;
            }

            return false;
        }


        /// <summary>
        /// Add items not already present to a collection, using the default
        /// identity rules of the collection and type.
        /// </summary>
        /// <typeparam name="T">Type of items in the collection.</typeparam>
        /// <param name="aCollection">Existing collection to add to.</param>
        /// <param name="aNewItems">New items to try adding to the collection.</param>
        /// <returns>The items that were NOT added to the collection.</returns>
        public static IList<T> AddUniqueFromRange<T>(this ICollection<T> aCollection, IEnumerable<T> aNewItems)
        {
            var notAdded = new List<T>();

            foreach (var item in aNewItems)
            {
                if (!AddIfUnique(aCollection, item))
                {
                    notAdded.Add(item);
                }
            }

            return notAdded;
        }
    }
}
