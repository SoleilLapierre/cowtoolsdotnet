﻿using CowTools.Reflection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CowTools.Reflection.Tests
{
    [TestFixture]
    [SetCulture("en-US")]
    public class TypeRegistryTests
    {
        [SetUp]
        public void Setup()
        {
            _reg = new TypeRegistry();
        }


        [Test]
        public void Defaults()
        {
            Assert.IsFalse(_reg.FindImplementationsOf<object>().Any());
            Assert.IsFalse(_reg.FindImplementationsOf(typeof(object)).Any());
            Assert.IsFalse(_reg.FindClassesWithAttribute<AttributeUsageAttribute>(true, false).Any());
        }


        [Test]
        public void NoAutomaticRegistration()
        {
            _reg.Register(typeof(TypeRegistryTests).Assembly);
            Assert.IsFalse(_reg.FindImplementationsOf<TypeRegistry>().Any());
            Assert.IsTrue(_reg.FindImplementationsOf<TypeRegistryTests>().Any());
        }


        [Test]
        public void FindOnlyConcreteClasses()
        {
            _reg.Register(typeof(TypeRegistryTests).Assembly);
            var types = new HashSet<Type>(_reg.FindImplementationsOf<I1>());
            Assert.IsTrue(types.Contains(typeof(I1Impl)));
            Assert.IsTrue(types.Contains(typeof(I2Impl)));
            Assert.IsTrue(types.Contains(typeof(I3Impl)));
            Assert.IsFalse(types.Contains(typeof(I1Abstract)));
        }


        [Test]
        public void FindExactInterface()
        {
            _reg.Register(typeof(TypeRegistryTests).Assembly);
            var types = new HashSet<Type>(_reg.FindImplementationsOf<I3>());
            Assert.IsFalse(types.Contains(typeof(I1Impl)));
            Assert.IsFalse(types.Contains(typeof(I2Impl)));
            Assert.IsTrue(types.Contains(typeof(I3Impl)));
            Assert.IsFalse(types.Contains(typeof(I1Abstract)));
        }


        private TypeRegistry _reg;
    }
}
