﻿using NUnit.Framework;
using System.Linq;

namespace CowTools.Reflection.Tests
{
    [TestFixture]
    [SetCulture("en-US")]
    public class AttributeFinderTests
    {
        [Test]
        public void FindBasic()
        {
            var attrs1 = AttributeFinder.FindClassAttributes<Test1Attribute>(typeof(TestClass1), false).ToArray();
            Assert.AreEqual(1, attrs1.Length);
            Assert.AreEqual("Foo", attrs1[0].Property1);

            var attrs2 = AttributeFinder.FindClassAttributes<Test2Attribute>(typeof(TestClass1), false).ToArray();
            Assert.AreEqual(0, attrs2.Length);
        }


        [Test]
        public void FindBothWays()
        {
            var attrs = AttributeFinder.FindClassAttributes<Test1Attribute>(typeof(TestClass1), false).ToArray();
            Assert.AreEqual(1, attrs.Length);
            Assert.AreEqual("Foo", attrs[0].Property1);

            attrs = AttributeFinder.FindClassAttributes<Test1Attribute>(new TestClass1(), false).ToArray();
            Assert.AreEqual(1, attrs.Length);
            Assert.AreEqual("Foo", attrs[0].Property1);
        }


        [Test]
        public void FindNonInherited()
        {
            var attrs = AttributeFinder.FindClassAttributes<Test1Attribute>(typeof(TestClass4), false).ToArray();
            Assert.AreEqual(1, attrs.Length);
            Assert.AreEqual("Foo3", attrs[0].Property1);

            attrs = AttributeFinder.FindClassAttributes<Test1Attribute>(typeof(TestClass3), false).ToArray();
            Assert.AreEqual(1, attrs.Length);
            Assert.AreEqual("Foo2", attrs[0].Property1);
        }


        [Test]
        public void FindInherited()
        {
            var attrs = AttributeFinder.FindClassAttributes<Test1Attribute>(typeof(TestClass4), true).ToArray();
            Assert.AreEqual(2, attrs.Length);
            Assert.AreEqual(1, attrs.Where(a => "Foo2" == a.Property1).Count());
            Assert.AreEqual(1, attrs.Where(a => "Foo3" == a.Property1).Count());
        }


        [Test]
        public void FindAttributeSubclasses()
        {
            var attrs = AttributeFinder.FindClassAttributes<Test2Attribute>(typeof(TestClass3), true).ToArray();
            Assert.AreEqual(2, attrs.Length);
            Assert.AreEqual(1, attrs.Where(a => "Bar2" == a.Property2).Count());
            Assert.AreEqual(1, attrs.Where(a => "Bar3" == a.Property2).Count());
            Assert.AreEqual(1, attrs.Where(a => a is Test3Attribute).Count());

            attrs = AttributeFinder.FindClassAttributes<Test3Attribute>(typeof(TestClass3), true).ToArray();
            Assert.AreEqual(1, attrs.Length);
            Assert.AreEqual("Bar3", attrs[0].Property2);
        }
    }
}
