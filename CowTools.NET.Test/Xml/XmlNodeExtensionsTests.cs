﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace CowTools.Xml.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class XmlNodeExtensionsTests
    {
        [SetUp]
        public void Setup()
        {
            var asm = Assembly.GetAssembly(typeof(XmlNodeExtensionsTests));
            _doc = new XmlDocument();
            _doc.Load(Path.Combine(Path.GetDirectoryName(asm.Location), "data", "Xml", "XmlExtensionsTestData.xml"));
            _rootNode = _doc.DocumentElement;
        }


        [Test]
        public void GetAllChildXmlNodes()
        {
            var nodes = _rootNode.GetAllChildXmlNodes().Select(n => n.Name).ToList();
            var expected = new List<string> { "Type1", "Type1", "Type2", "Type2" };
            CollectionAssert.AreEqual(expected, nodes);
        }


        [Test]
        public void FindChildXmlNodesStr()
        {
            var nodes = _rootNode.FindChildXmlNodes("Type1").ToList();
            Assert.AreEqual(2, nodes.Count);
        }


        [Test]
        public void FindChildXmlNodes()
        {
            var nodes = _rootNode.FindChildXmlNodes(n => n.Name == "Type2").ToList();
            Assert.AreEqual(2, nodes.Count);
            nodes = _rootNode.FindChildXmlNodes(n => n.Name.Contains("Type")).ToList();
            Assert.AreEqual(4, nodes.Count);
        }


        [Test]
        public void GetAttributeOrNull()
        {
            var nodes = _rootNode.GetAllChildXmlNodes().Select(n => n.GetAttributeOrNull("id")).ToList();
            var expected = new List<string> { "id1", "id2", "id3", "id4" };
            CollectionAssert.AreEqual(expected, nodes);
            nodes = _rootNode.GetAllChildXmlNodes().Select(n => n.GetAttributeOrNull("name")).ToList();
            expected = new List<string> { "Foo", "Bar", "Baz", "Quux" };
            CollectionAssert.AreEqual(expected, nodes);
            nodes = _rootNode.GetAllChildXmlNodes().Select(n => n.GetAttributeOrNull("occasional")).ToList();
            expected = new List<string> { null, null, "here", null };
            CollectionAssert.AreEqual(expected, nodes);
        }


        private XmlDocument _doc;
        private XmlNode _rootNode;
    }
}
