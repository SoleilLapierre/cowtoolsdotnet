﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CowTools.Reflection
{
    /// <summary>
    /// Helper class for finding types in assemblies that meet 
    /// specified conditions, such as implementing some interface
    /// or having certain attributes. 
    /// This class is thread safe by virtue of internal locking.
    /// </summary>
    /// <remarks>Currently unloading an assembly is not supported;
    /// work around this by creating a new instance and repopulating
    /// it with the kept assemblies.</remarks>
    public class TypeRegistry
    {
        /// <summary>
        /// Register one or more assemblies as type sources. If scans for types
        /// of interest have already been performed, they will be updated with
        /// results from the new assemblies.
        /// </summary>
        /// <param name="assemblies">Assemblies to scan for types.</param>
        public void Register(params Assembly[] assemblies)
            => Register(assemblies as IEnumerable<Assembly>);


        /// <summary>
        /// Register one or more assemblies as type sources. If scans for types
        /// of interest have already been performed, they will be updated with
        /// results from the new assemblies.
        /// </summary>
        /// <param name="assemblies">Assemblies to scan for types.</param>
        public void Register(IEnumerable<Assembly> assemblies)
        {
            if (assemblies is null)
            {
                throw new ArgumentNullException(nameof(assemblies));
            }

            lock (_syncRoot)
            {
                foreach (var asm in assemblies)
                {
                    if (!_assemblies.Contains(asm))
                    {
                        _assemblies.Add(asm);

                        foreach (var pair in _implementations)
                        {
                            pair.Value.AddRange(GetImplementationsFromAssembly(asm, pair.Key));
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Scan all registered assemblies for instantiable classes that
        /// either implement or subclass the given type. The type argument
        /// can itself be instantiable but the intended use is to find
        /// implementations of an interface or abstract base class.
        /// </summary>
        /// <typeparam name="T">Type to scan for implementations of.</typeparam>
        /// <returns>Zero or more instantiable types.</returns>
        public IEnumerable<Type> FindImplementationsOf<T>()
            => FindImplementationsOf(typeof(T));


        /// <summary>
        /// Scan all registered assemblies for instantiable classes that
        /// either implement or subclass the given type. The type argument
        /// can itself be instantiable but the intended use is to find
        /// implementations of an interface or abstract base class.
        /// </summary>
        /// <param name="type">Type to scan for implementations of.</param>
        /// <returns>Zero or more instantiable types.</returns>
        public IEnumerable<Type> FindImplementationsOf(Type type)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            IEnumerable<Type> result = new Type[0];

            lock (_syncRoot)
            {
                if (_implementations.TryGetValue(type, out var list))
                {
                    result = list.ToArray();
                }
                else
                {
                    var newList = new List<Type>();

                    foreach (var asm in _assemblies)
                    {
                        newList.AddRange(GetImplementationsFromAssembly(asm, type));
                    }

                    _implementations[type] = newList;
                    result = newList.ToArray();
                }
            }

            return result;
        }


        /// <summary>
        /// Find classes in all registered assemblies that have the specified attribute type.
        /// </summary>
        /// <typeparam name="T">Type of attribute to search for.</typeparam>
        /// <param name="inherit">Include base classes.</param>
        /// <param name="concrete">Return only instantiable types.</param>
        /// <param name="filter">Optional filter for attribute values; class will be included
        /// in the results of one of its attributes results in a true return from this predicate.</param>
        /// <returns>Zero or more matching types.</returns>
        public IEnumerable<Type> FindClassesWithAttribute<T>(bool inherit, bool concrete, Predicate<T> filter = null) where T: Attribute
        {
            lock (_syncRoot)
            {
                foreach (var asm in _assemblies)
                {
                    foreach (var type in asm.GetExportedTypes())
                    {
                        if (!concrete || !type.IsAbstract)
                        {
                            foreach (var attr in AttributeFinder.FindClassAttributes<T>(type, inherit))
                            {
                                if ((filter is null) || filter.Invoke(attr))
                                {
                                    yield return type;
                                }
                            }
                        }
                    }
                }
            }
        }


        private static IEnumerable<Type> GetImplementationsFromAssembly(Assembly asm, Type type)
        {
            return asm.GetExportedTypes().Where(t => !t.IsAbstract && type.IsAssignableFrom(t));
        }


        private object _syncRoot = new object();
        private Dictionary<Type, List<Type>> _implementations = new Dictionary<Type, List<Type>>();
        private HashSet<Assembly> _assemblies = new HashSet<Assembly>();
    }
}
