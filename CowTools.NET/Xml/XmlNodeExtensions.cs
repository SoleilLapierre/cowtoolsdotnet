﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace CowTools.Xml
{
    /// <summary>
    /// Extension methods for <see cref="XmlNode"/>
    /// </summary>
    public static class XmlNodeExtensions
    {
        /// <summary>
        /// Find all child nodes that are of type <see cref="XmlNode"/>.
        /// </summary>
        /// <param name="aNode">Node to search children of.</param>
        /// <returns>Zero or more child nodes, in the order found.</returns>
        public static IEnumerable<XmlNode> GetAllChildXmlNodes(this XmlNode aNode)
        {
            foreach (var nodeXml in aNode.ChildNodes)
            {
                if (nodeXml is XmlNode node)
                {
                    yield return node;
                }
            }
        }


        /// <summary>
        /// Find all child nodes of type <see cref="XmlNode"/> that match a given predicate.
        /// </summary>
        /// <param name="aNode">Node to search children of.</param>
        /// <param name="aFilter">Filtering function.</param>
        /// <returns>Zero or more child nodes matching the filter, in the order found.</returns>
        public static IEnumerable<XmlNode> FindChildXmlNodes(this XmlNode aNode, Predicate<XmlNode> aFilter)
        {
            return aNode.GetAllChildXmlNodes().Where(n => aFilter(n));
        }


        /// <summary>
        /// Find all child nodes of type <see cref="XmlNode"/> that have a given value of <see cref="XmlNode.Name"/>.
        /// </summary>
        /// <param name="aNode">Node to search children of.</param>
        /// <param name="aName">Name to search for.</param>
        /// <returns>Zero or more child nodes matching the filter, in the order found.</returns>
        public static IEnumerable<XmlNode> FindChildXmlNodes(this XmlNode aNode, string aName)
        {
            return aNode.FindChildXmlNodes(n => n.Name == aName);
        }


        /// <summary>
        /// Get the value of an <see cref="XmlNode"/> attribute by name if it exists;
        /// returns null otherwise.
        /// </summary>
        /// <param name="aNode">Node to search attributes of.</param>
        /// <param name="aAttrName">Name of the attribute to find.</param>
        /// <returns>Value of the attribute, or null if it does not exist.</returns>
        public static string GetAttributeOrNull(this XmlNode aNode, string aAttrName)
        {
            try
            {
                return aNode.Attributes[aAttrName].Value;
            }
            catch (NullReferenceException) { }
            catch (InvalidCastException) { }

            return null;
        }
    }
}
