﻿using System;

namespace CowTools.Reflection.Tests
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class Test1Attribute : Attribute
    {
        public Test1Attribute(string propertyValue)
        {
            Property1 = propertyValue;
        }

        public string Property1 { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class Test2Attribute : Attribute
    {
        public Test2Attribute(string propertyValue)
        {
            Property2 = propertyValue;
        }

        public string Property2 { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class Test3Attribute : Test2Attribute
    {
        public Test3Attribute(string property2Value, string property3Value)
        : base(property2Value)
        {
            Property3 = property3Value;
        }

        public string Property3 { get; private set; }
    }
}
