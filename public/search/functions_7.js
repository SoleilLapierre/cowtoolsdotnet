var searchData=
[
  ['register_0',['register',['../classCowTools_1_1Reflection_1_1TypeRegistry.html#a5e0ab769a7f3f15e61bc5f1851f0cbb9',1,'CowTools.Reflection.TypeRegistry.Register(params Assembly[] assemblies)'],['../classCowTools_1_1Reflection_1_1TypeRegistry.html#a5a8738a8a957efded1b93e68250db0cb',1,'CowTools.Reflection.TypeRegistry.Register(IEnumerable&lt; Assembly &gt; assemblies)']]],
  ['remove_1',['remove',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#ae840ef1f6f78912d93fbd5837691aa16',1,'CowTools.Collections.LayeredDictionary.Remove(TKey key)'],['../classCowTools_1_1Collections_1_1LayeredDictionary.html#a4512a64a62b5b173a59adebb054f95ee',1,'CowTools.Collections.LayeredDictionary.Remove(KeyValuePair&lt; TKey, TValue &gt; item)']]],
  ['removefromlayer_2',['RemoveFromLayer',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#a2449d75165333a3b7e0acdbb88171fed',1,'CowTools::Collections::LayeredDictionary']]],
  ['removelayer_3',['removelayer',['../classCowTools_1_1Collections_1_1LayeredDictionary.html#ad93277f225c06210698c6e49916a7a75',1,'CowTools.Collections.LayeredDictionary.RemoveLayer()'],['../classCowTools_1_1Collections_1_1LayeredDictionary.html#a3a248fcd6a37a6edcf51e456ad259e52',1,'CowTools.Collections.LayeredDictionary.RemoveLayer(int layer)']]]
];
