﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CowTools.Expressions
{
    /// <summary>
    /// Simple algebraic expression tree. 
    /// Can parse simple expressions like "4 * (3 + x)" from a string into
    /// a tree structure and then later evaluate the expression given
    /// values for the variables. Handles postfix and infix forms but not prefix.
    /// 
    /// Does not support function calls or unary operators.
    /// Supports numeric operators + - * / ^ = != &gt; &lt;
    /// Supports local operators &gt; &lt; = != &amp; | !&amp; (nand) !| (nor)
    /// Parentheses are only supported in infix notation.
    /// Logical operators take inputs >= 0.5 to be true, and output either 0 or 1.
    /// 
    /// Currently only supports double values but may be changed to generic in future.
    /// </summary>
    public class ArithmeticTree
    {
        /// <summary>
        /// Constant value for this tree node.
        /// Only leaf nodes should have a value or a variable name, and should only have one of those.
        /// </summary>
        public double? Value { get; set; }

        /// <summary>
        /// Variable name for this node. Used to dynamically look up a numeric value at evaluation time.
        /// If this is set, it is used in preference to any constant value.
        /// Only leaf nodes should have a value or a variable name, and should only have one of those.
        /// </summary>
        public string Variable { get; set; }


        /// <summary>
        /// Operator of this node. Must only be set for interior nodes.
        /// Legal values: +-*/^
        /// </summary>
        public string Operator { get; set; } = null;


        /// <summary>
        /// LHS of the operator in this node. Should only be set when there is an operator.
        /// </summary>
        public ArithmeticTree Left { get; set; }


        /// <summary>
        /// RHS of the operator in this node. Should only be set when there is an operator.
        /// </summary>
        public ArithmeticTree Right { get; set; }


        /// <summary>
        /// Evaluate the expression represented by this node as the tree root.
        /// </summary>
        /// <param name="variables">Lookup table for variables.</param>
        /// <returns>The value of the expression given the current values of the variables.</returns>
        public double Evaluate(IReadOnlyDictionary<string, double> variables = null)
        {
            if (string.IsNullOrEmpty(Operator))
            {
                if (!string.IsNullOrEmpty(Variable))
                {
                    if (variables is null)
                    {
                        throw new InvalidOperationException($"Expression contains a variable {Variable} but no table of values was provided.");
                    }

                    return variables[Variable];
                }

                if (Value.HasValue)
                {
                    return Value.Value;
                }

                throw new InvalidOperationException("Expression value node has no constant value or variable name.");
            }

            if (Left is null || Right is null)
            {
                throw new InvalidOperationException("Expression operator nodes must have two children.");
            }

            var left = Left.Evaluate(variables);
            var right = Right.Evaluate(variables);
            switch(Operator)
            {
                case "+":
                    return left + right;
                case "-":
                    return left - right;
                case "*":
                    return left * right;
                case "/":
                    return left / right;
                case "^":
                    return Math.Pow(left, right);
                case "<":
                    return left < right ? 1 : 0;
                case ">":
                    return left > right ? 1 : 0;
                case "<=":
                    return left <= right ? 1 : 0;
                case ">=":
                    return left >= right ? 1 : 0;
                case "=":
                    return left == right ? 1 : 0;
                case "!=":
                    return left != right ? 1 : 0;
                case "&":
                    return (left >= 0.5) && (right >= 0.5) ? 1 : 0;
                case "|":
                    return (left >= 0.5) || (right >= 0.5) ? 1 : 0;
                case "!&":
                    return (left >= 0.5) && (right >= 0.5) ? 0 : 1;
                case "!|":
                    return (left >= 0.5) || (right >= 0.5) ? 0 : 1;
                default:
                    throw new InvalidOperationException($"Invalid expression operator '{Operator}'");
            }
        }


        /// <summary>
        /// Parses a string into an expression tree.
        /// </summary>
        /// <param name="input">String to parse.</param>
        /// <returns>Root node of the resulting expression tree.</returns>
        public static ArithmeticTree Parse(string input)
        {
            var match = _tokenizer.Match(input);
            var tokens = match.Groups[1].Captures.Cast<Capture>().Select(cap => cap.Value);
            var output = new List<string>();
            var stack = new Stack<string>();

            // Implementation of the Shunting Yard algorithm
            // https://en.wikipedia.org/wiki/Shunting_yard_algorithm
            foreach (var token in tokens)
            {
                if (double.TryParse(token, out var value))
                {
                    output.Add(token);
                    continue;
                }

                if (bool.TryParse(token, out var bvalue))
                {
                    output.Add(token);
                    continue;
                }

                if (!_operators.Contains(token) && !_parens.Contains(token))
                {
                    output.Add(token);
                    continue;
                }

                if (_operators.Contains(token))
                {
                    while (stack.Count > 0)
                    {
                        var tos = stack.Peek();
                        if (!_parens.Contains(tos) &&
                            ((_precedence[tos] > _precedence[token]) || (!_rightOps.Contains(token) && _precedence[tos] == _precedence[token])))
                        {
                            stack.Pop();
                            output.Add(tos);
                        }
                        else
                        {
                            break;
                        }
                    }

                    stack.Push(token);
                    continue;
                }

                if (token == "(")
                {
                    stack.Push(token);
                    continue;
                }

                if (token == ")")
                {
                    while (stack.Peek() != "(")
                    {
                        output.Add(stack.Pop());
                    }

                    if (stack.Pop() != "(")
                    {
                        throw new ArgumentException("Mismatched parentheses.");
                    }

                    continue;
                }
            }

            while (stack.Count > 0)
            {
                var token = stack.Pop();
                if (token == "(")
                {
                    throw new ArgumentException("Mismatched parentheses.");
                }

                output.Add(token);
            }

            return ParsePostfixTokens(output);
        }


        /// <summary>
        /// Parse an expression given in postfix (RPN) form. 
        /// </summary>
        /// <param name="input">RPM expression to parse.</param>
        /// <returns>The root of the corresponding tree.</returns>
        public static ArithmeticTree ParsePostfix(string input)
        {
            var tokens = _tokenizer.Match(input);
            return ParsePostfixTokens(tokens.Groups[1].Captures.Cast<Capture>().Select(cap => cap.Value));
        }


        private static ArithmeticTree ParsePostfixTokens(IEnumerable<string> tokens)
        { 
            var stack = new Stack<ArithmeticTree>();

            foreach (var token in tokens)
            {
                if (_operators.Contains(token))
                {
                    var right = stack.Pop();
                    var left = stack.Pop();
                    var opNode = new ArithmeticTree() { Operator = token, Left = left, Right = right };
                    stack.Push(opNode);
                    continue;
                }

                if (double.TryParse(token, out var num))
                {
                    var constNode = new ArithmeticTree() { Value = num };
                    stack.Push(constNode);
                    continue;
                }

                if (bool.TryParse(token, out var bvalue))
                {
                    var constNode = new ArithmeticTree() { Value = bvalue ? 1 : 0 };
                    stack.Push(constNode);
                    continue;
                }

                var varNode = new ArithmeticTree() {  Variable = token.Trim() };
                stack.Push(varNode);
            }

            if (stack.Count != 1)
            {
                throw new ArgumentException($"Postfix expression does not produce a single result. {stack.Count} items left at end of parsing.");
            }

            return stack.Pop();
        }


        private static Regex _tokenizer = new Regex("(?:([\\d\\.]+|\\w+|\\!=|\\!\\&|\\!\\||<=|>=|[-+=*/^<>=\\&\\|]|[\\(\\)])\\s?)*", RegexOptions.Compiled | RegexOptions.Multiline);
        private static string[] _operators = { "+", "-", "*", "/", "^", "<", ">", "<=", ">=", "=", "!=", "&", "|", "!&", "!|" };
        private static string[] _rightOps = { "^" };
        private static string[] _parens = { "(", ")" };
        private static IReadOnlyDictionary<string, int> _precedence = new Dictionary<string, int>
        {
            { "^", 4 },
            { "*", 3 },
            { "/", 3 },
            { "&", 3 },
            { "!&", 3 },
            { "+", 2 },
            { "-", 2 },
            { "|", 2 },
            { "!|", 2 },
            { "=", 1 },
            { "!=", 1 },
            { "<", 1 },
            { ">", 1 },
            { ">=", 1 },
            { "<=", 1 },
        };
    }
}
