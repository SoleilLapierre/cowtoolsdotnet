# CowTools.NET

This package exists to reduce the frequency with which I rewrite the same basic
functionality in my C# programs. 

## NO SUPPORT

This is a personal project; no support is provided, content is subject to radical change without 
notice, and it may be abandoned or deleted at any time.

## Functionality

General functionality provided:
* Reflection tools to find attributes and implementations of interfaces.

## API Documentation

API documentation can be viewed by cloning the repository and opening `public/index.html` in your 
web browser.


